# Boosts

Publish to your Mastodon server 2021 top three more boosted posts of your known fediverse.  

### Dependencies

-   **Python 3**
-   Postgresql server
-   [Mastodon](https://joinmastodon.org) server admin

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed Python libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed tables in it.  

3. Run `python setup.py` to get your Mastodon's bot account tokens.

4. Use your favourite scheduling method to set `python boosts.py` to run regularly.

