import pdb
import sys
import os
import os.path
import re
from datetime import datetime, timedelta
from mastodon import Mastodon
import psycopg2

def get_boosts(current_year):

    try:

        conn = None

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select count(reblog_of_id) as boosts, reblog_of_id from statuses where date_part('year', created_at) = (%s) group by reblog_of_id order by boosts desc limit 3", (str(current_year),))

        rows = cur.fetchall()

        for row in rows:

            boosts_count_lst.append(row[0])

            boosts_id_lst.append(row[1])

        cur.close()

        return (boosts_count_lst, boosts_id_lst)

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def get_uri(status_id):

    try:

        conn = None

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select uri from statuses where id = (%s)", (status_id,))

        row = cur.fetchone()

        status_uri = row[0]

        cur.close()

        return (status_uri)

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def save_status(status_id, boost_count, status_uri, current_year):

    try:

        conn = None

        conn = psycopg2.connect(database = boosted_db, user = boosted_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select status_id from boosts where status_id = (%s)", (status_id,))

        row = cur.fetchone()

        if row == None:

            insert_sql = "insert into boosts(status_id, boosts_count, status_uri, boosts_year) values(%s, %s, %s, %s) ON CONFLICT DO NOTHING"

            cur.execute(insert_sql, (status_id, boost_count, status_uri, current_year))

            conn.commit()

        else:

            update_sql = "update boosts set boosts_count=(%s) where status_id=(%s)"

            cur.execute(update_sql, (boost_count, status_id))

            conn.commit()

        conn.close()


    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def mastodon():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)
    bot_username = get_parameter("bot_username", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    return (mastodon, mastodon_hostname, bot_username)

def db_config():

    # Load db configuration from config file
    config_filepath = "config/db_config.txt"
    mastodon_db = get_parameter("mastodon_db", config_filepath)
    mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
    boosted_db = get_parameter("boosted_db", config_filepath)
    boosted_db_user = get_parameter("boosted_db_user", config_filepath)

    return (mastodon_db, mastodon_db_user, boosted_db, boosted_db_user)

def get_parameter( parameter, file_path ):

    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def usage():

    print('usage: python ' + sys.argv[0] + ' --run')

###############################################################################
# main

if __name__ == '__main__':

    # usage modes

    if len(sys.argv) == 1:

        usage()

    elif len(sys.argv) >= 2:

        if sys.argv[1] == '--run':

            mastodon, mastodon_hostname, bot_username = mastodon()

            mastodon_db, mastodon_db_user, boosted_db, boosted_db_user = db_config()

            today = datetime.today()

            current_year = today.year

            boosts_count_lst = []

            boosts_id_lst = []

            boosts_count_lst, boosts_id_lst = get_boosts(current_year)

            status_uris = []

            toot_text = "\nFediverse's " + str(current_year) + " top three most boosted posts :\n\n"

            i = 0

            while i < len(boosts_count_lst):

                status_id = boosts_id_lst[i]

                status_uri = get_uri(status_id)

                boost_count = boosts_count_lst[i]

                toot_text += "boosts: " + str(boost_count) + "\n" + status_uri + "\n\n"

                save_status(status_id, boost_count, status_uri, current_year)

                i += 1

            print(toot_text)

            mastodon.status_post(toot_text, in_reply_to_id=None,)

        else:

            usage()
